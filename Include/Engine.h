#ifndef __ENGINE_H_
#define __ENGINE_H_

#include <Windows.h>
#include <Addresses.h>
#include <InterfaceMacros-win32.h>
#include <d3d9.h>

namespace Engine
{
	FN(HWND, CreateGameWindow, (int xRight, int yBottom, char a3), ENGINE_CREATEGAMEWINDOW_ADDR);
	FN(int, DeviceNotify, (void* a1), ENGINE_DEVICENOTIFY_ADDR);
	FNSTDCALL(LRESULT, GameWndProc, (HWND, UINT, WPARAM, LPARAM), ENGINE_GAMEWNDPROC_ADDR);
	FN(void, SetupDisplaySettings, (), ENGINE_SETUPDISPLAYSETTINGS);

	FN(int, sprintfThing, (char* a1, ...), ENGINE_VSNPRINTFTHING);

	FN(int, debugout, (void* pThis, int a2, char* a3, ...), ENGINE_DEBUGOUT);
	FN(char, LoadTexBundle, (int* a1, int* a2, int* a3, char a4), ENGINE_LOADTEXBUNDLE);
	FN(int, printAptDebug, (char* fmt, ...), ENGINE_PRINTAPTDEBUG);

	FN(char*, translateIDString, (char* buffer, __int64 input), ENGINE_TRANSLATEIDSTRING);

	FN(void, setMapMatrix, (), ENGINE_SETMAPMATRIX);

	class DebugInfo
	{
	};

	class UnknownClass
	{
	};

	class IEFrame
	{
	public:
		MEMBERFN(int, CloseIEFrame, (char a1), (a1), ENGINE_CLOSEIEFRAME);
	};

	class LocationData
	{
	public:
		char gap0[0xA0];
		float xPos;
		float yPos;
		float zPos;
	};

	class PlayerData
	{
	public:
		char gap0[0x1C47A0];
		LocationData* locationData;
	};

	class RoadThing
	{
	public:
		char gap0[0x2A0];
	};

	class RoadThing2
	{
	public:
		char gap0[0x2A0];
	};

	class AptThing
	{
	public:
		int unk0;
		int unk4;
		int unk8;
		int unk12;
		int unk16;
		int unk20;
		int unk24;
		int unk28;
		int unk32;
	};

	namespace Members
	{
		RAWMEMBERFN(DebugInfo, void, WriteDebugString, (LPCSTR str), ENGINE_WRITEDEBUGSTRING);
		RAWMEMBERFN(UnknownClass, char, LoadWorldTex, (int a1, char* a2), ENGINE_LOADWORLDTEX);
		RAWMEMBERFN(IEFrame, char, ShowIEFrame, (const char* a1, int a3, int a4, int a5), ENGINE_SHOWIEFRAME);
		RAWMEMBERFN(IEFrame, int, CloseIEFrame, (char a1), ENGINE_CLOSEIEFRAME);
	};

	GLOBALVAR(bool, isFullscreen, ENGINE_ISFULLSCREEN_ADDR);

	GLOBALVAR(float, mapScaleX, ENGINE_MAPSCALEX_ADDR);
	GLOBALVAR(float, mapScaleY, ENGINE_MAPSCALEY_ADDR);
	GLOBALVAR(float, mapPositionX, ENGINE_MAPPOSITIONX_ADDR);
	GLOBALVAR(float, mapPositionY, ENGINE_MAPPOSITIONY_ADDR);

	GLOBALVAR(PlayerData*, playerData, ENGINE_PLAYERDATA_ADDR);

	GLOBALVAR(IDirect3DDevice9*, D3DDevice, ENGINE_D3DDEVICE_ADDR);

	GLOBALVAR(HWND, theWindow, ENGINE_WINDOW_ADDR);

	GLOBALVAR(AptThing*, aptUndefined, ENGINE_APT_UNDEFINED_ADDR);

}

#endif // __ENGINE_H_