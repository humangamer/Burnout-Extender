#ifndef __MAP_MATRIX_H_
#define __MAP_MATRIX_H_

#include "FuncInterceptor.h"

extern void initMapMatrixMod(CodeInjection::FuncInterceptor* hook);

#endif /* __MAP_MATRIX_H_ */
