#ifndef __DISTRICTS_H_
#define __DISTRICTS_H_

#include "FuncInterceptor.h"

void initDistrictsMod(CodeInjection::FuncInterceptor* hook);

#endif // __DISTRICTS_H_
