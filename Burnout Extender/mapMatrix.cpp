#include "mapMatrix.h"

#include "Engine.h"
#include "FuncInterceptor.h"

void(*originalSetMapMatrix)() = Engine::setMapMatrix;

void newSetMapMatrix()
{
	originalSetMapMatrix();

	Engine::mapScaleX = 10200.0f; //11117.040; //8817.040f;
	Engine::mapScaleY = 9718.0f; //7834.971f;//8834.971f;
	Engine::mapPositionX = -4148.0f; //-5059.117f;
	Engine::mapPositionY = -5858.0f; //-5230.067;
}

void initMapMatrixMod(CodeInjection::FuncInterceptor* hook)
{
	originalSetMapMatrix = hook->intercept(Engine::setMapMatrix, newSetMapMatrix);
}
