#include <Windows.h>

#include "Addresses.h"
#include "CodeAllocator.h"
#include "CodeInjectionStream.h"
#include "FuncInterceptor.h"
#include "util.h"
#include "mods.h"

namespace
{
	CodeInjection::CodeAllocator *codeAlloc;
	CodeInjection::CodeInjectionStream *injectionStream;
	CodeInjection::FuncInterceptor *hook;

	bool initCore()
	{
		codeAlloc = new CodeInjection::CodeAllocator();
		injectionStream = new CodeInjection::CodeInjectionStream(reinterpret_cast<void*>(BP_TEXT_START), BP_TEXT_SIZE);
		hook = new CodeInjection::FuncInterceptor(injectionStream, codeAlloc);

		return true;
	}

	bool InitBurnoutExtension()
	{
		if (!initCore())
			return false;

		if (!initMods(hook))
			return false;

		return true;
	}
}

extern "C" __declspec(dllexport) bool LoadBurnoutExtension()
{
	const char* testPointer = reinterpret_cast<const char*>(0xA811D0);
	const char* testStr = "ea_burnout_ultimate_pc";

	if (!test_string_pointer(testPointer, testStr))
		return false;

	return InitBurnoutExtension();
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		if (!LoadBurnoutExtension())
		{
			MessageBox(nullptr, "Failed to initialize Burnout Extension.\n\nMake sure you are using the steam executable!", "Error", MB_OK | MB_ICONERROR);
			TerminateProcess(GetCurrentProcess(), 0);
		}
		break;
	case DLL_PROCESS_DETACH:
		break;
	}

	return true;
}