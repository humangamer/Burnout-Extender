#include "saveInjection.h"

#include "SaveData.h"
#include "util.h"
#include "FuncInterceptor.h"

SaveData SaveDataInstance;

void init_extra_save_data(char* gameData)
{
	SaveDataInstance.Init(gameData);
}

void load_extra_save_data(char* gameData, char* saveData)
{
	SaveDataInstance.Load(gameData, saveData);
}

void write_extra_save_data(char* gameData, char* saveData)
{
	SaveDataInstance.Save(gameData, saveData);
}

__declspec(naked) void init_save()
{
	__asm
	{
		push esi;
		call init_extra_save_data;
		pop esi;

		pop edi;
		mov[esi + 0AB8Ch], ebx;
		mov[esi + 0AB90h], ebx;
		mov[esi + 0AB94h], ebx;
		mov[esi + 0AB98h], ebx;
		mov[esi + 0AB9Ch], ebx;
		mov[esi + 0ABA0h], ebx;
		pop ebx;
		mov esp, ebp;
		pop ebp;
		retn;
	}
}

__declspec(naked) void load_save()
{
	__asm
	{
		push edi;
		push esi;
		call load_extra_save_data;
		pop esi;
		pop edi;

		pop esi;
		pop ebp;
		pop ebx;
		add esp, 38h;
		retn 4h;
	}
}

__declspec(naked) void write_save()
{
	__asm
	{
		//mov ebx, [esp + 38h];
		push ebx;
		push esi;
		call write_extra_save_data;
		pop esi;
		pop ebx;

		pop edi;
		pop esi;
		pop ebp;
		pop ebx;
		add esp, 30h;
		retn 0Ch;
	}
}

void initSaveDataMod(CodeInjection::FuncInterceptor* hook)
{
	inject_jmp(0x50E26B, &init_save);
	inject_jmp(0x51112A, &load_save);
	inject_jmp(0x510130, &write_save);
}
