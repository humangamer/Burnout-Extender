#include "util.h"

#include <Windows.h>
#include "CodeInjectionStream.h"

bool test_string_pointer(const char *testPointer, const char *testStr)
{
	// Make sure we can actually read from the test memory location
	MEMORY_BASIC_INFORMATION memInfo;
	if (VirtualQuery(testPointer, &memInfo, sizeof(memInfo)) != sizeof(memInfo))
		return false;
	if (memInfo.Protect == 0 || (memInfo.Protect & PAGE_NOACCESS) || (memInfo.Protect & PAGE_EXECUTE))
		return false;

	// Check if the string matches
	if (memcmp(testPointer, testStr, strlen(testStr)) != 0)
		return false;
	return true;
}

void inject_jmp(int address, void(*func))
{
	void* initPtr = reinterpret_cast<void*>(address);
	CodeInjection::CodeInjectionStream stream(initPtr, 6);
	stream.writeRel32Jump(func);
	stream.flush();
}
