#include "districts.h"

#include "FuncInterceptor.h"
#include "util.h"

char const* districts[24]
{
	"OceanView",
	"WestAcres",
	"TwinBridges",
	"BigSurfBeach",
	"EasternShore",
	"HillsidePass",
	"HeartbreakHills",
	"RockridgeCliffs",
	"SouthBay",
	"ParkVale",
	"ParadiseWharf",
	"CristalSummit",
	"LonePeaks",
	"SunsetValley",
	"Downtown",
	"RiverCity",
	"MotorCity",
	"Waterfront",
	"Island1",
	"Island2",
	"Island3",
	"OceanView", // Island4
	"BigSurfBeach", // Island5
	"DistrictInvalid"
};

char const* counties[6]
{
	"PalmBay",
	"SilverLake",
	"HarborTown",
	"WhiteMountain",
	"Downtown",
	"ParadiseKeys"
};

int mapDistricts(const int district)
{
	switch(district)
	{
	case 0:
	case 1:
	case 2:
	case 3:
		return 0;
	case 4:
	case 5:
	case 6:
		return 1;
	case 7:
	case 8:
	case 9:
	case 10:
		return 2;
	case 11:
	case 12:
	case 13:
		return 3;
	case 14:
	case 15:
	case 16:
	case 17:
		return 4;
	case 18:
	case 19:
	case 20:
	case 21:
	case 22:
		return 5;
	default:
		return 6;
	}
}

void __declspec(naked) mapDistricts_asm()
{
	__asm
	{
		push edx;
		push ecx;
		push eax;
		call mapDistricts;
		add esp, 4;
		pop ecx;
		pop edx;

		retn;
	}
}

void initDistrictsMod(CodeInjection::FuncInterceptor* hook)
{
	// districts
	void* addrDistricts = &districts[0];

	void* initPtr1 = reinterpret_cast<void*>(0x55AB14);
	CodeInjection::CodeInjectionStream stream1(initPtr1, 4);
	stream1.write(&addrDistricts, 4);
	stream1.flush();

	void* initPtr2 = reinterpret_cast<void*>(0x55ABCF);
	CodeInjection::CodeInjectionStream stream2(initPtr2, 4);
	stream2.write(&addrDistricts, 4);
	stream2.flush();

	void* initPtr3 = reinterpret_cast<void*>(0x55AC7D);
	CodeInjection::CodeInjectionStream stream3(initPtr3, 4);
	stream3.write(&addrDistricts, 4);
	stream3.flush();
	
	void* initPtr4 = reinterpret_cast<void*>(0x4B2DA7);
	CodeInjection::CodeInjectionStream stream4(initPtr4, 4);
	stream4.write(&addrDistricts, 4);
	stream4.flush();

	// mappings
	inject_jmp(0x700300, mapDistricts_asm);
}
