#include "worldTex4.h"

#include <Windows.h>

void initWorldTex4Mod(CodeInjection::FuncInterceptor* hook)
{
	void* ptr = reinterpret_cast<void*>(0xA7E0F0);
	CodeInjection::CodeInjectionStream stream(ptr, 24);
	char* text = "WORLDTEX4.BIN";
	stream.write(text, strlen(text) + 1); // add 1 because of null terminating byte
	stream.flush();

	// Load WORLDTEX4.BIN by replacing char* at 0xA7E0F0
	/*void* ptr = reinterpret_cast<void*>(0xA7E0F0);
	DWORD oldProtect;
	VirtualProtect(ptr, 24, PAGE_EXECUTE_READWRITE, &oldProtect);

	char* text = reinterpret_cast<char*>(ptr);
	text = "WORLDTEX4.BIN";

	DWORD oldProtect2;
	VirtualProtect(ptr, 24, oldProtect, &oldProtect2);*/
}
