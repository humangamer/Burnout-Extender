#ifndef __SAVE_DATA_H_
#define __SAVE_DATA_H_

#include "types.h"

class SaveData
{
public:
	SaveData();
	~SaveData();

	void Init(char* gameData);
	void Load(char* gameData, char* saveData);
	void Save(char* gameData, char* saveData);

private:

	VehicleID lastUsedCarID;
	VehicleID lastUsedBikeID;

	U32 numPCPDVehicleIDs;
	VehicleID PCPDVehicleIDs[35];
	U16 PCPDVehicleIndices[35];
	U8 PCPDVehicleColors[35];
	U8 PCPDVehicleColorTypes[35];
	U32 PCPDVehicleMileage[35];

	struct Vehicle
	{
		VehicleID ID;
		U8 Color;
		U8 ColorType;
		U8 Unknown1;
		U8 Unknown2;
		F32 Damage;
		U32 Type;
	};

	Vehicle BSIVehicleIDs[33];
	U32 BSIVehicleMileage[33];

	struct Event
	{
		U32 ID;
		U16 Unknown1;
		U16 Null;
	};

	U32 numBSIEvents;
	Event BSIEvents[15];

	U64 BSIBillboardIDs[45];
	U32 numBSIBillboardsDone;

	U64 BSISmashIDs[75];
	U32 numBSIShashesDone;

	U64 BSIMegaJumpIDs[15];
	U32 numBSIMegaJumpsDone;

	U32 BSIDrivethruID1;
	U32 BSIDrivethruID2;

	U32 BSIGasStationsFoundID1;
	U32 BSIGasStationsFoundID2;

	U32 BSIDrivethruID3;

	U64 BSIChallengeIDs[10];
	U32 numBSIChallengesDone;

	struct FriendRoadRule
	{
		U64 Unknown1;
		U64 Unknown2;
		U32 TimeRoadRuleScore;
		U32 ShowTimeRoadRuleScore;
		char TimeRoadRuleUserName[20];
		char ShowTimeRoadRuleUserName[20];
	};

	FriendRoadRule FriendsBSIRoadRules[12];

	struct RoadRule
	{
		U64 Unknown1;
		U64 RoadCompletion;
		U32 TimeRoadRuleScore;
		U32 ShowTimeRoadRuleScore;
		VehicleID TimeRoadRuleVehicleID;
		VehicleID ShowTimeRoadRuleVehicleID;
	};

	RoadRule BSIRoadRules[12];

	struct BikeRoadRuleVehicles
	{
		VehicleID DayVehicleID;
		VehicleID NightVehicleID;
	};

	BikeRoadRuleVehicles BSIBikeRoadRulesVehicles[12];

	struct BikeRoadRuleTimes
	{
		U64 DayTime;
		U64 NightTime;
	};

	BikeRoadRuleTimes BSIBikeRoadRuleTimes[12];

	struct FriendBikeRoadRuleNames
	{
		char DayName[20];
		char NightName[20];
	};

	FriendBikeRoadRuleNames BSIFriendBikeRoadRuleNames[12];
};

#endif /* __SAVE_DATA_H_ */
