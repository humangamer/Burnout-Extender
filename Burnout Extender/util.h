#ifndef __UTIL_H_
#define __UTIL_H_

extern bool test_string_pointer(const char *testPointer, const char *testStr);

extern void inject_jmp(int address, void(*func));

#endif /* __UTIL_H_ */
