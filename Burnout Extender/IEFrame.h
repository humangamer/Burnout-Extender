#ifndef __IE_FRAME_H_
#define __IE_FRAME_H_

#include "FuncInterceptor.h"

void initIEFrameMod(CodeInjection::FuncInterceptor* hook);

#endif /* __IE_FRAME_H_ */