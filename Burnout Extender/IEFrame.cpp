#include "IEFrame.h"

#include <cstdio>

#include "Engine.h"
#include "FuncInterceptor.h"

THISFN(void, newShowIEFrame, (Engine::IEFrame *pThis, const char* a1, int a3, int a4, int a5))
{
	printf("Skipping IE Frame");
	pThis->CloseIEFrame(1);
}

void initIEFrameMod(CodeInjection::FuncInterceptor* hook)
{
	typedef void(*tmpfnptr_t)(); // The two pointers need to be casted to something, since intercept doesn't allow two void*'s for safety reasons

	void* ShowIEFramePtr = reinterpret_cast<void*>(Engine::Members::ShowIEFrame);
	tmpfnptr_t originalFn2 = reinterpret_cast<tmpfnptr_t>(ShowIEFramePtr);
	tmpfnptr_t newFn2 = reinterpret_cast<tmpfnptr_t>(newShowIEFrame);
	ShowIEFramePtr = reinterpret_cast<void*>(hook->intercept(originalFn2, newFn2));

	// Don't resize window! (pointless because of above but might as well leave it)
	char* data = reinterpret_cast<char*>(0x7E4E6A);
	data[0] = 0xEB;
	data[1] = 0x1F;
}
