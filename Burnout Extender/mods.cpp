#include "mods.h"
#include "FuncInterceptor.h"
#include "Debug.h"
#include "windowedMode.h"
#include "mapMatrix.h"
#include "worldTex4.h"
#include "saveInjection.h"
#include "IEFrame.h"
#include "utilities.h"
#include "roadrules.h"
#include "districts.h"

bool initMods(CodeInjection::FuncInterceptor* hook)
{
	initDebugMod(hook);
	initWindowedModeMod(hook);
	initMapMatrixMod(hook);
	initIEFrameMod(hook);
	//initWorldTex4Mod(hook); // currently doesn't seem to work
	initSaveDataMod(hook);
	initUtilitiesMod(hook);
	initRoadRulesMod(hook);
	initDistrictsMod(hook);

	return true;
}
