#ifndef __WORLDTEX4_H_
#define __WORLDTEX4_H_

#include "FuncInterceptor.h"

extern void initWorldTex4Mod(CodeInjection::FuncInterceptor* hook);

#endif /* __WORLDTEX4_H_ */
