#ifndef __MODS_H_
#define __MODS_H_

#include "FuncInterceptor.h"

extern bool initMods(CodeInjection::FuncInterceptor* hook);

#endif /* __MODS_H_ */
