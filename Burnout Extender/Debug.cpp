#include "Debug.h"

#include <cstdarg>
#include <stdio.h>

#include "Engine.h"
#include "FuncInterceptor.h"


int(*originalDebugOut)(void* pThis, int a2, char* a3, ...) = Engine::debugout;

int newDebugOut(void* pThis, int a2, char* a3, ...)
{
	va_list va;
	va_start(va, a3);
	_vprintf_p(a3, va);
	printf("\n");

	return originalDebugOut(pThis, a2, a3);
}

int(*originalPrintAptDebug)(char* fmt, ...) = Engine::printAptDebug;

int newPrintAptDebug(char* fmt, ...)
{
	char v2[2048];
	va_list va;
	va_start(va, fmt);
	_vprintf_p(fmt, va);
	//printf("\n");
	return _vsnprintf(v2, 2048, fmt, va);
}

THISFN(void, newWriteDebugString, (Engine::DebugInfo *pThis, LPCSTR str))
{
	printf("%s", str);
}

void initDebugMod(CodeInjection::FuncInterceptor* hook)
{
	originalDebugOut = hook->intercept(Engine::debugout, newDebugOut);

	originalPrintAptDebug = hook->intercept(Engine::printAptDebug, newPrintAptDebug);

	typedef void(*tmpfnptr_t)(); // The two pointers need to be casted to something, since intercept doesn't allow two void*'s for safety reasons

	void* writeDebugStringPtr = reinterpret_cast<void*>(Engine::Members::WriteDebugString);
	tmpfnptr_t originalFn = reinterpret_cast<tmpfnptr_t>(writeDebugStringPtr);
	tmpfnptr_t newFn = reinterpret_cast<tmpfnptr_t>(newWriteDebugString);
	writeDebugStringPtr = reinterpret_cast<void*>(hook->intercept(originalFn, newFn));
}
