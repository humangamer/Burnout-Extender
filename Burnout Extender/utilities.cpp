#include "utilities.h"
#include "FuncInterceptor.h"

#include <Windows.h>

#include <d3d9.h>
#include <d3dx9.h>
#include <Dwmapi.h> 

#include <thread>

#include "util.h"

#include "Engine.h"

/*HHOOK winHook;

LRESULT CALLBACK CBTProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	HMODULE d3d9 = GetModuleHandleA("d3d9.dll");
	if (d3d9)
	{
		
	}

	return CallNextHookEx(winHook, nCode, wParam, lParam);
}*/

//typedef IDirect3D9* (*WINAPI D3DCreate9Func)(UINT SDKVersion);

//void HookD3D9(HMODULE hModule)
//{
//	/*FARPROC presentProc = GetProcAddress(hModule, "IDirect3DDevice9::Present");
//	if (!presentProc)
//	{
//		MessageBox(nullptr, "Failed to hook IDirect3DDevice9::Present!", "Error", MB_OK | MB_ICONERROR);
//		return;
//	}
//
//	MessageBox(nullptr, "Successfully hooked IDirect3DDevice9::Present!", "DEBUG", MB_OK | MB_ICONWARNING);*/
//
//	LPDIRECT3D9(__stdcall*pDirect3DCreate9)(UINT) = (LPDIRECT3D9(__stdcall*)(UINT))GetProcAddress(hModule, "Direct3DCreate9");
//	LPDIRECT3D9 pD3D = pDirect3DCreate9(D3D_SDK_VERSION);
//	
//	D3DDISPLAYMODE d3ddm;
//	HRESULT hRes = pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);
//	D3DPRESENT_PARAMETERS d3dpp;
//	ZeroMemory(&d3dpp, sizeof(d3dpp));
//	d3dpp.Windowed = true;
//	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
//	d3dpp.BackBufferFormat = d3ddm.Format;
//
//	IDirect3DDevice9 * ppReturnedDeviceInterface;
//
//	HWND hWnd = FindWindowA(NULL, "Your window");
//
//	hRes = pD3D->CreateDevice(
//		D3DADAPTER_DEFAULT,
//		D3DDEVTYPE_HAL,
//		hWnd,
//		D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_DISABLE_DRIVER_MANAGEMENT,
//		&d3dpp, &ppReturnedDeviceInterface);
//
//	if (hRes != D3D_OK)
//	{
//		MessageBox(nullptr, "Failed to get device", "Error", MB_OK | MB_ICONERROR);
//		return;
//	}
//
//	MessageBox(nullptr, "Successfully got device!", "DEBUG", MB_OK | MB_ICONWARNING);
//}

//void RenderOverlay(IDirect3DDevice9* device)
//{
//	printf("BLABLABLA: %d", device);
//}
//
//unsigned char original1;
//unsigned int original2;
//
//unsigned char new1;
//unsigned int new2;

//struct pointerToMember
//{
//	/* This field has separate representations for non-virtual and virtual
//	functions.  For non-virtual functions, this field is simply the
//	address of the function.  For our case, virtual functions, this
//	field is 1 plus the virtual table offset (in bytes) of the function
//	in question.  The least-significant bit therefore discriminates
//	between virtual and non-virtual functions.
//
//	"Ah," you say, "what about architectures where function pointers do
//	not necessarily have even addresses?"  (ARM, MIPS, and AArch64 are
//	the major ones.)  Excellent point.  Please see below.  */
//	size_t pointerOrOffset;
//
//	/* This field is only interesting for calling the function; it
//	describes the amount that the `this' pointer must be adjusted
//	prior to the call.  However, on architectures where function
//	pointers do not necessarily have even addresses, this field has the
//	representation:
//
//	2 * adjustment + (virtual_function_p ? 1 : 0)  */
//	ptrdiff_t thisAdjustment;
//};
//
//void* find_f_address(IDirect3DDevice9* aClass)
//{
//	/* The virtual function table is stored at the beginning of the object.  */
//	void** vtable = *(void***)aClass;
//
//	/* This structure is described in the cross-platform "Itanium" C++ ABI:
//
//	http://mentorembedded.github.io/cxx-abi/abi.html
//
//	The particular layout replicated here is described in:
//
//	http://mentorembedded.github.io/cxx-abi/abi.html#member-pointers  */
//
//	/* Translate from the opaque pointer-to-member type representation to
//	the representation given above.  */
//	pointerToMember p;
//	int((IDirect3DDevice9::*m)()) = reinterpret_cast<int((IDirect3DDevice9::*)())>(&IDirect3DDevice9::Present);
//	memcpy(&p, &m, sizeof(p));
//
//	/* Compute the actual offset into the vtable.  Given the differing meaing
//	of the fields between architectures, as described above, and that
//	there's no convenient preprocessor macro, we have to do this
//	ourselves.  */
//#if defined(__arm__) || defined(__mips__) || defined(__aarch64__)
//	/* No adjustment required to `pointerOrOffset'.  */
//	static const size_t pfnAdjustment = 0;
//#else
//	/* Strip off the lowest bit of `pointerOrOffset'.  */
//	static const size_t pfnAdjustment = 1;
//#endif
//
//	size_t offset = (p.pointerOrOffset - pfnAdjustment) / sizeof(void*);
//
//	/* Now grab the address out of the vtable and return it.  */
//	return vtable[offset];
//}

//HRESULT __cdecl presentHook(const RECT* pSourceRect, const RECT* pDestRect, HWND hDestWindowOverride, const RGNDATA* pDirtyRegion)
//{
//	IDirect3DDevice9* device;
//	__asm
//	{
//		mov device, ecx;
//	}
//
//	RenderOverlay(device);
//
//
//	void* presentFn = reinterpret_cast<void*>(reinterpret_cast<int*>(&device) + 17 * 4);
//
//
//	void* ptr = reinterpret_cast<void*>(presentFn);
//	DWORD oldProtect;
//	VirtualProtect(ptr, 6, PAGE_EXECUTE_READWRITE, &oldProtect);
//
//	//reinterpret_cast<int*>(device)[17]
//	// device->Present
//	//unsigned char* ip = reinterpret_cast<unsigned char*>(device->Present);
//	unsigned char* ip = reinterpret_cast<unsigned char*>(presentFn);
//	*ip = original1;
//	*(reinterpret_cast<int*>(ip + 1)) = original2;
//
//	HRESULT result = device->Present(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
//
//	*ip = new1;
//	*(reinterpret_cast<int*>(ip + 1)) = new2;
//
//	DWORD oldProtect2;
//	VirtualProtect(ptr, 6, oldProtect, &oldProtect2);
//
//	return result;
//}
//
//void initUtil()
//{
//	IDirect3DDevice9* device = Engine::D3DDevice;
//
//	//typedef void(*FnPtr)();
//	//FnPtr *vtable = *(FnPtr **)&(IDirect3DDevice9 &)device;
//	//FnPtr presentFn = *(vtable[(int)17]);
//
//	//void* presentFn = reinterpret_cast<void*>(reinterpret_cast<int*>(&device) + 17 * 4);
//	void* presentFn = reinterpret_cast<void*>(reinterpret_cast<int*>(&device) + 17 * 4);
//
//
//	//int presentAddr = reinterpret_cast<int>(device->Present);
//	int presentAddr = reinterpret_cast<int>(presentFn);
//	int presentOverride = reinterpret_cast<int>(&presentHook);
//
//	int offset = presentOverride - presentAddr - 5;
//
//	const unsigned char jmp = 0xE9;
//
//	void* ptr = reinterpret_cast<void*>(presentFn);
//	DWORD oldProtect;
//	VirtualProtect(ptr, 6, PAGE_EXECUTE_READWRITE, &oldProtect);
//
//	//unsigned char* ip = reinterpret_cast<unsigned char*>(device->Present);
//	unsigned char* ip = reinterpret_cast<unsigned char*>(presentFn);
//	original1 = *ip;
//	new1 = jmp;
//	*ip = jmp;
//	original2 = *(reinterpret_cast<int*>(ip + 1));
//	new2 = offset;
//	*(reinterpret_cast<int*>(ip + 1)) = offset;
//
//	DWORD oldProtect2;
//	VirtualProtect(ptr, 6, oldProtect, &oldProtect2);
//}
//

//int s_width = 800;
//int s_height = 600;
//
//LPDIRECT3D9 d3d;    // the pointer to our Direct3D interface
//LPDIRECT3DDEVICE9 d3ddev;
//
//HWND hWnd;
//const MARGINS  margin = { 0,0,s_width,s_height };
//LPD3DXFONT pFont;
//
//void initD3D(HWND hWnd)
//{
//	d3d = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface
//
//	D3DPRESENT_PARAMETERS d3dpp;    // create a struct to hold various device information
//
//	ZeroMemory(&d3dpp, sizeof(d3dpp));    // clear out the struct for use
//	d3dpp.Windowed = TRUE;    // program windowed, not fullscreen
//	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;    // discard old frames
//	d3dpp.hDeviceWindow = hWnd;    // set the window to be used by Direct3D
//	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;     // set the back buffer format to 32-bit
//	d3dpp.BackBufferWidth = s_width;    // set the width of the buffer
//	d3dpp.BackBufferHeight = s_height;    // set the height of the buffer
//
//	d3dpp.EnableAutoDepthStencil = TRUE;
//	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
//
//	// create a device class using this information and the info from the d3dpp stuct
//	HRESULT result = d3d->CreateDevice(D3DADAPTER_DEFAULT,
//		D3DDEVTYPE_HAL,
//		hWnd,
//		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
//		&d3dpp,
//		&d3ddev);
//
//	if (result != D3D_OK)
//	{
//		MessageBox(NULL, "Failed to init D3D9", "Error", MB_OK | MB_ICONERROR);
//		ExitProcess(0);
//	}
//
//	D3DXCreateFont(d3ddev, 50, 0, FW_BOLD, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial", &pFont);
//
//}
//
//void DrawString(int x, int y, DWORD color, LPD3DXFONT g_pFont, const char *fmt)
//{
//	RECT FontPos = { x, y, x + 120, y + 16 };
//	char buf[1024] = { '\0' };
//	va_list va_alist;
//
//	va_start(va_alist, fmt);
//	vsprintf_s(buf, fmt, va_alist);
//	va_end(va_alist);
//	g_pFont->DrawText(NULL, buf, -1, &FontPos, DT_NOCLIP, color);
//}
//
//void render()
//{
//	// clear the window alpha
//	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.0f, 0);
//
//	d3ddev->BeginScene();    // begins the 3D scene
//
//
//	DrawString(10, 50, D3DCOLOR_ARGB(255, 255, 0, 0), pFont, "Test rendering string :D");
//
//
//
//	d3ddev->EndScene();    // ends the 3D scene
//
//	d3ddev->Present(NULL, NULL, NULL, NULL);   // displays the created frame on the screen
//}
//
//LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
//{
//	switch (message)
//	{
//	case WM_PAINT:
//	{
//		DwmExtendFrameIntoClientArea(hWnd, &margin);
//	}break;
//
//	case WM_DESTROY:
//	{
//		PostQuitMessage(0);
//		return 0;
//	} break;
//	}
//
//	return DefWindowProc(hWnd, message, wParam, lParam);
//}
//
//HWND newhwnd;
//void thing()
//{
//	//Engine::theWindow;//
//	const char* value = "Untitled - Notepad";
//	RECT rc;
//	newhwnd = FindWindow(NULL, value);
//	if (newhwnd != NULL) {
//		GetWindowRect(newhwnd, &rc);
//		s_width = rc.right - rc.left;
//		s_height = rc.bottom - rc.top;
//	}
//	else {
//		ExitProcess(0);
//	}
//
//	WNDCLASSEX wc;
//
//	ZeroMemory(&wc, sizeof(WNDCLASSEX));
//
//	wc.cbSize = sizeof(WNDCLASSEX);
//	wc.style = CS_HREDRAW | CS_VREDRAW;
//	wc.lpfnWndProc = WindowProc;
//	//wc.hInstance = hInstance;
//	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
//	wc.hbrBackground = (HBRUSH)RGB(0, 0, 0);
//	wc.lpszClassName = "WindowClass";
//
//	RegisterClassEx(&wc);
//
//	hWnd = CreateWindowEx(0,
//		"WindowClass",
//		"",
//		WS_EX_TOPMOST | WS_POPUP,
//		rc.left, rc.top,
//		s_width, s_height,
//		NULL,
//		NULL,
//		NULL,//hInstance,
//		NULL);
//
//	SetWindowLong(hWnd, GWL_EXSTYLE, (int)GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_TRANSPARENT);
//	SetLayeredWindowAttributes(hWnd, RGB(0, 0, 0), 0, ULW_COLORKEY);
//	SetLayeredWindowAttributes(hWnd, 0, 255, LWA_ALPHA);
//
//	ShowWindow(hWnd, 1);
//
//
//	initD3D(hWnd);
//	//MSG msg;
//	::SetWindowPos(newhwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
//	
//}
//
//void thing2()
//{
//	MSG msg;
//	while (true)
//	{
//		::SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
//
//		if (!newhwnd)
//			ExitProcess(0);
//		render();
//		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
//		{
//			TranslateMessage(&msg);
//			DispatchMessage(&msg);
//		}
//
//		if (msg.message == WM_QUIT)
//			exit(0);
//
//
//	}
//}

void initUtil()
{
	//thing();
	//std::thread pickle(thing2);
}

void* initUtilBack = reinterpret_cast<void*>(0x945B00);

__declspec(naked) void init_util()
{
	__asm
	{
		//push eax;
		call initUtil;
		//pop eax;

		call    initUtilBack;
		xor     edi, edi;

		push 947FDBh;
		ret;
	}
}

void initUtilitiesMod(CodeInjection::FuncInterceptor* hook)
{
	//HMODULE mod = GetModuleHandle(nullptr);
	//winHook = SetWindowsHookEx(WH_CBT, &CBTProc, nullptr, 0);// mod, 0);

	/*HMODULE d3d9 = GetModuleHandleA("d3d9.dll");
	if (!d3d9)
	{
		MessageBox(nullptr, "Failed to hook into DirectX!", "Error", MB_OK | MB_ICONERROR);
		return;
	}

	HookD3D9(d3d9);*/

	inject_jmp(0x947FD4, &init_util);


}

