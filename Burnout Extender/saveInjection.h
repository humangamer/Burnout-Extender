#ifndef __SAVE_INJECTION_H_
#define __SAVE_INJECTION_H_

#include "FuncInterceptor.h"

class SaveData;
extern SaveData SaveDataInstance;

extern void initSaveDataMod(CodeInjection::FuncInterceptor* hook);

#endif /* __SAVE_INJECTION_H_ */
