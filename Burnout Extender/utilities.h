#ifndef __UTILITIES_H_
#define __UTILITIES_H_

#include "FuncInterceptor.h"

extern void initUtilitiesMod(CodeInjection::FuncInterceptor* hook);

#endif /* __UTILITIES_H_ */
