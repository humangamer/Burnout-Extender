#ifndef __ROADRULES_H_
#define __ROADRULES_H_

#include "FuncInterceptor.h"

extern void initRoadRulesMod(CodeInjection::FuncInterceptor* hook);

#endif /* __ROADRULES_H_ */
