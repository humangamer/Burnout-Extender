#ifndef __WINDOWED_MODE_H_
#define __WINDOWED_MODE_H_
#include "FuncInterceptor.h"

extern void initWindowedModeMod(CodeInjection::FuncInterceptor* hook);

#endif /* __WINDOWED_MODE_H_ */
