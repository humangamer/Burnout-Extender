#ifndef __DEBUG_H_
#define __DEBUG_H_

#include "FuncInterceptor.h"

extern void initDebugMod(CodeInjection::FuncInterceptor* hook);

#endif /* __DEBUG_H_ */
