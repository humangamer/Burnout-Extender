#include "windowedMode.h"

#include "FuncInterceptor.h"
#include "Engine.h"

void(*originalSetupDisplaySettings)() = Engine::SetupDisplaySettings;

void newSetupDisplaySettings()
{
	originalSetupDisplaySettings();

	Engine::isFullscreen = false;
}

void initWindowedModeMod(CodeInjection::FuncInterceptor* hook)
{
	originalSetupDisplaySettings = hook->intercept(Engine::SetupDisplaySettings, newSetupDisplaySettings);
}
