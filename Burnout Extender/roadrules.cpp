#include "roadrules.h"
#include "util.h"
#include "Engine.h"

void doRoadOverride(Engine::RoadThing* roadThing, int roadIndex)
{
	printf("-----> Road Override(1) At: %d\n", roadIndex);
}

void doRoadOverride2(Engine::RoadThing2* roadThing2, int roadIndex)
{
	printf("-----> Road Override(2) At: %d\n", roadIndex);
}

double doAptOverride(Engine::AptThing* aptthing)
{
	Engine::PlayerData* player = Engine::playerData;
	Engine::LocationData* location = player->locationData;
	if (location != nullptr)
		printf("Position: (%g, %g, %g)\n", location->xPos, location->yPos, location->zPos);

	if (aptthing == nullptr)
	{
		printf("Error: APT Pointer is null!\n");
		return 1.0;
	}

	if (~(unsigned __int8)((unsigned int)aptthing->unk4 >> 4) & 1)
	{
		return 0.0;
	} else
	{
		int unk = aptthing->unk4 >> 25;
		switch(unk)
		{
		case 1:
		case 33:
			if (unk != 1)
				aptthing = (Engine::AptThing*)aptthing->unk32;
			return atof((const char*)(aptthing->unk8 + 8));
		case 5:
			if (LOBYTE(aptthing->unk8))
				return 1.0;
			else
				return 0.0;
		case 7:
			return (double)aptthing->unk8;
		case 6:
			return *(float*)&aptthing->unk8;
		default:
			if (aptthing == Engine::aptUndefined)
				return 0.0;
			else
				return 1.0;
		}
	}
}

void doAptOverride2(Engine::AptThing* aptthing)
{
	if (aptthing == nullptr)
	{
		printf("Error2: Null AptThing!");
	}
}

void doAptOverride3(Engine::AptThing* aptthing)
{
	if (aptthing == nullptr)
	{
		printf("Error3: Null AptThing!");
	}
}

__declspec(naked) void roadOverride()
{
	__asm
	{
		push ebp;
		push esi;
		call doRoadOverride;
		pop esi;
		pop ebp;

		push 4E8C82h;
		ret;
	}
}

__declspec(naked) void roadOverride2()
{
	__asm
	{
		push eax;
		push esi;
		call doRoadOverride2;
		pop esi;
		pop eax;

		push 624B94h;
		ret;
	}
}

__declspec(naked) void aptOverride()
{
	__asm
	{
		push ecx;
		call doAptOverride;
		pop ecx;

		retn;
	}
}

__declspec(naked) void aptOverride2()
{
	__asm
	{
		mov edx, [esp + 12d];

		push edx;
		call doAptOverride2;
		pop edx;

		mov eax, 0;// [edx + 24d];

		push 7DFF04h;
		ret;
	}
}

__declspec(naked) void aptOverride3()
{
	__asm
	{
		mov edx, [esp + 8d];

		push edx;
		call doAptOverride3;
		pop edx;

		mov eax, 0;// [edx + 24d];

		push 7DFFFDh;
		ret;
	}
}

__declspec(naked) void streetOverride()
{
	__asm
	{
		add esi, 1d;
		cmp esi, 4Ch; // street count

		push 4C1EFCh;
		ret;
	}
}

void initRoadRulesMod(CodeInjection::FuncInterceptor* hook)
{
	/*inject_jmp(0x4E8C7B, &roadOverride);
	inject_jmp(0x624B5B, &roadOverride2);
	*/
	
	
	//inject_jmp(0x89A0D0, &aptOverride);
	
	
	/*inject_jmp(0x7DFEFD, &aptOverride2);
	inject_jmp(0x7DFFF6, &aptOverride3);

	inject_jmp(0x4C1EF6, &streetOverride);*/
}