#include "SaveData.h"

#include <cstring>

SaveData::SaveData()
{
	
}

SaveData::~SaveData()
{
	
}

void SaveData::Init(char* game_data)
{
	lastUsedCarID = 0;
	lastUsedBikeID = 0;

	numPCPDVehicleIDs = 0;

	memset(PCPDVehicleIDs, 0x00, sizeof(PCPDVehicleIDs));
	memset(PCPDVehicleIndices, 0x00, sizeof(PCPDVehicleIndices));
	memset(PCPDVehicleColors, 0x00, sizeof(PCPDVehicleColors));
	memset(PCPDVehicleColorTypes, 0x00, sizeof(PCPDVehicleColorTypes));
	memset(PCPDVehicleMileage, 0x00, sizeof(PCPDVehicleMileage));

	memset(BSIVehicleIDs, 0x00, sizeof(BSIVehicleIDs));
	memset(BSIVehicleMileage, 0x00, sizeof(BSIVehicleMileage));

	numBSIEvents = 0;
	memset(BSIEvents, 0x00, sizeof(BSIEvents));

	memset(BSIBillboardIDs, 0x00, sizeof(BSIBillboardIDs));
	numBSIBillboardsDone = 0;

	memset(BSISmashIDs, 0x00, sizeof(BSISmashIDs));
	numBSIShashesDone = 0;

	memset(BSIMegaJumpIDs, 0x00, sizeof(BSIMegaJumpIDs));
	numBSIMegaJumpsDone = 0;

	BSIDrivethruID1 = 0;
	BSIDrivethruID2 = 0;

	BSIGasStationsFoundID1 = 0;
	BSIGasStationsFoundID2 = 0;

	BSIDrivethruID3 = 0;

	memset(BSIChallengeIDs, 0x00, sizeof(BSIChallengeIDs));
	numBSIChallengesDone = 0;

	memset(FriendsBSIRoadRules, 0x00, sizeof(FriendsBSIRoadRules));
	memset(BSIRoadRules, 0x00, sizeof(BSIRoadRules));
	memset(BSIBikeRoadRulesVehicles, 0x00, sizeof(BSIBikeRoadRulesVehicles));
	memset(BSIBikeRoadRuleTimes, 0x00, sizeof(BSIBikeRoadRuleTimes));
	memset(BSIFriendBikeRoadRuleNames, 0x00, sizeof(BSIFriendBikeRoadRuleNames));
}

void SaveData::Load(char* game_data, char* save_data)
{
	int offset = 0x2FE78;

	lastUsedCarID = *reinterpret_cast<VehicleID*>(save_data + offset);
	offset += 8;
	lastUsedBikeID = *reinterpret_cast<VehicleID*>(save_data + offset);
	offset += 8;

	// PCPD
	numPCPDVehicleIDs = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	for (int i = 0; i < 35; i++)
	{
		PCPDVehicleIDs[i] = *reinterpret_cast<VehicleID*>(save_data + offset);
		offset += 8;
	}

	for (int i = 0; i < 35; i++)
	{
		PCPDVehicleIndices[i] = *reinterpret_cast<U16*>(save_data + offset);
		offset += 2;
	}

	for (int i = 0; i < 35; i++)
	{
		PCPDVehicleColors[i] = *reinterpret_cast<U8*>(save_data + offset);
		offset += 1;
	}

	for (int i = 0; i < 35; i++)
	{
		PCPDVehicleColorTypes[i] = *reinterpret_cast<U8*>(save_data + offset);
		offset += 1;
	}

	for (int i = 0; i < 35; i++)
	{
		PCPDVehicleMileage[i] = *reinterpret_cast<U32*>(save_data + offset);
		offset += 4;
	}

	// Island
	for (int i = 0; i < 33; i++)
	{
		BSIVehicleIDs[i].ID = *reinterpret_cast<VehicleID*>(save_data + offset);
		offset += 8;
		BSIVehicleIDs[i].Color = *reinterpret_cast<U8*>(save_data + offset);
		offset += 1;
		BSIVehicleIDs[i].ColorType = *reinterpret_cast<U8*>(save_data + offset);
		offset += 1;
		BSIVehicleIDs[i].Unknown1 = *reinterpret_cast<U8*>(save_data + offset);
		offset += 1;
		BSIVehicleIDs[i].Unknown2 = *reinterpret_cast<U8*>(save_data + offset);
		offset += 1;
		BSIVehicleIDs[i].Damage = *reinterpret_cast<F32*>(save_data + offset);
		offset += 4;
		BSIVehicleIDs[i].Type = *reinterpret_cast<U32*>(save_data + offset);
		offset += 4;
	}

	for (int i = 0; i < 33; i++)
	{
		BSIVehicleMileage[i] = *reinterpret_cast<U32*>(save_data + offset);
		offset += 4;
	}

	numBSIEvents = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	for (int i = 0; i < 15; i++)
	{
		BSIEvents[i].ID = *reinterpret_cast<U32*>(save_data + offset);
		offset += 4;
		BSIEvents[i].Unknown1 = *reinterpret_cast<U16*>(save_data + offset);
		offset += 2;
		BSIEvents[i].Null = *reinterpret_cast<U16*>(save_data + offset);
		offset += 2;
	}

	for (int i = 0; i < 45; i++)
	{
		BSIBillboardIDs[i] = *reinterpret_cast<U64*>(save_data + offset);
		offset += 8;
	}

	numBSIBillboardsDone = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	for (int i = 0; i < 75; i++)
	{
		BSISmashIDs[i] = *reinterpret_cast<U64*>(save_data + offset);
		offset += 8;
	}

	numBSIShashesDone = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	for (int i = 0; i < 15; i++)
	{
		BSIMegaJumpIDs[i] = *reinterpret_cast<U64*>(save_data + offset);
		offset += 8;
	}

	numBSIMegaJumpsDone = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	BSIDrivethruID1 = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	BSIDrivethruID2 = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	BSIGasStationsFoundID1 = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	BSIGasStationsFoundID2 = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	BSIDrivethruID3 = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	for (int i = 0; i < 10; i++)
	{
		BSIChallengeIDs[i] = *reinterpret_cast<U64*>(save_data + offset);
		offset += 8;
	}

	numBSIChallengesDone = *reinterpret_cast<U32*>(save_data + offset);
	offset += 4;

	// TODO: Finish Load
}

void SaveData::Save(char* game_data, char* save_data)
{
	int offset = 0x2FE78;

	*reinterpret_cast<VehicleID*>(save_data + offset) = lastUsedCarID;
	offset += 8;
	*reinterpret_cast<VehicleID*>(save_data + offset) = lastUsedBikeID;
	offset += 8;

	// PCPD
	*reinterpret_cast<U32*>(save_data + offset) = numPCPDVehicleIDs;
	offset += 4;

	for (int i = 0; i < 35; i++)
	{
		*reinterpret_cast<VehicleID*>(save_data + offset) = PCPDVehicleIDs[i];
		offset += 8;
	}

	for (int i = 0; i < 35; i++)
	{
		*reinterpret_cast<U16*>(save_data + offset) = PCPDVehicleIndices[i];
		offset += 2;
	}

	for (int i = 0; i < 35; i++)
	{
		*reinterpret_cast<U8*>(save_data + offset) = PCPDVehicleColors[i];
		offset += 1;
	}

	for (int i = 0; i < 35; i++)
	{
		*reinterpret_cast<U8*>(save_data + offset) = PCPDVehicleColorTypes[i];
		offset += 1;
	}

	for (int i = 0; i < 35; i++)
	{
		*reinterpret_cast<U32*>(save_data + offset) = PCPDVehicleMileage[i];
		offset += 4;
	}

	// Island
	for (int i = 0; i < 33; i++)
	{
		*reinterpret_cast<VehicleID*>(save_data + offset) = BSIVehicleIDs[i].ID;
		offset += 8;
		*reinterpret_cast<U8*>(save_data + offset) = BSIVehicleIDs[i].Color;
		offset += 1;
		*reinterpret_cast<U8*>(save_data + offset) = BSIVehicleIDs[i].ColorType;
		offset += 1;
		*reinterpret_cast<U8*>(save_data + offset) = BSIVehicleIDs[i].Unknown1;
		offset += 1;
		*reinterpret_cast<U8*>(save_data + offset) = BSIVehicleIDs[i].Unknown2;
		offset += 1;
		*reinterpret_cast<F32*>(save_data + offset) = BSIVehicleIDs[i].Damage;
		offset += 4;
		*reinterpret_cast<U32*>(save_data + offset) = BSIVehicleIDs[i].Type;
		offset += 4;
	}

	for (int i = 0; i < 33; i++)
	{
		*reinterpret_cast<U32*>(save_data + offset) = BSIVehicleMileage[i];
		offset += 4;
	}

	*reinterpret_cast<U32*>(save_data + offset) = numBSIEvents;
	offset += 4;

	for (int i = 0; i < 15; i++)
	{
		*reinterpret_cast<U32*>(save_data + offset) = BSIEvents[i].ID;
		offset += 4;
		*reinterpret_cast<U16*>(save_data + offset) = BSIEvents[i].Unknown1;
		offset += 2;
		*reinterpret_cast<U16*>(save_data + offset) = BSIEvents[i].Null;
		offset += 2;
	}

	for (int i = 0; i < 45; i++)
	{
		*reinterpret_cast<U64*>(save_data + offset) = BSIBillboardIDs[i];
		offset += 8;
	}

	*reinterpret_cast<U32*>(save_data + offset) = numBSIBillboardsDone;
	offset += 4;

	for (int i = 0; i < 75; i++)
	{
		*reinterpret_cast<U64*>(save_data + offset) = BSISmashIDs[i];
		offset += 8;
	}

	*reinterpret_cast<U32*>(save_data + offset) = numBSIShashesDone;
	offset += 4;

	for (int i = 0; i < 15; i++)
	{
		*reinterpret_cast<U64*>(save_data + offset) = BSIMegaJumpIDs[i];
		offset += 8;
	}

	*reinterpret_cast<U32*>(save_data + offset) = numBSIMegaJumpsDone;
	offset += 4;

	*reinterpret_cast<U32*>(save_data + offset) = BSIDrivethruID1;
	offset += 4;

	*reinterpret_cast<U32*>(save_data + offset) = BSIDrivethruID2;
	offset += 4;

	*reinterpret_cast<U32*>(save_data + offset) = BSIGasStationsFoundID1;
	offset += 4;

	*reinterpret_cast<U32*>(save_data + offset) = BSIGasStationsFoundID2;
	offset += 4;

	*reinterpret_cast<U32*>(save_data + offset) = BSIDrivethruID3;
	offset += 4;

	for (int i = 0; i < 10; i++)
	{
		*reinterpret_cast<U64*>(save_data + offset) = BSIChallengeIDs[i];
		offset += 8;
	}

	*reinterpret_cast<U32*>(save_data + offset) = numBSIChallengesDone;
	offset += 4;

	// TODO: Finish Save
}
